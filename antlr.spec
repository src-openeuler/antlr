%global debug_package %{nil}

Name:          antlr
Version:       2.7.7
Release:       60
Summary:       A parser generator that uses LL(*) for parsing
Epoch:         0
License:       public domain
URL:           http://www.antlr2.org/
Source0:       https://www.antlr2.org/download/antlr-2.7.7.tar.gz
Source1:       %{name}-build.xml
Source2:       %{name}-script
Source3:       http://repo2.maven.org/maven2/antlr/antlr/%{version}/%{name}-%{version}.pom
Patch0001:     antlr-2.7.7-newgcc.patch
Patch0002:     antlr-examples-license.patch

BuildRequires: gcc-c++ make ant java-devel >= 1:1.7.0 java-javadoc javapackages-local
BuildRequires: doxygen graphviz

%description
ANTLR (ANother Tool for Language Recognition) is a powerful parser generator for reading,
processing, executing, or translating structured text or binary files.

%package       tool
Summary:       A parser generator that uses LL(*) for parsing
Provides:      %{name} = %{version}-%{release}
BuildArch:     noarch

%description   tool
ANTLR (ANother Tool for Language Recognition) is a powerful parser generator for reading,
processing, executing, or translating structured text or binary files.

%package       help
Summary:       Help document for antlr
Provides:      %{name}-manual = %{epoch}:%{version}-%{release} %{name}-javadoc = %{epoch}:%{version}-%{release}
Obsoletes:     %{name}-manual < %{epoch}:%{version}-%{release} %{name}-javadoc < %{epoch}:%{version}-%{release}
BuildArch:     noarch

%description   help
Help document for antlr.

%package       C++
Summary:       C++ library for antlr generated parsers
Provides:      antlr-static = %{version}-%{release}

%description   C++
This package provides a static C++ library for parsers.

%package       C++-doc
Summary:       Documentation for C++ library
BuildArch:     noarch

%description   C++-doc
This package contains the documentation for C++ library.

%prep
%autosetup -n %{name}-%{version} -p1
find . -name "*.jar" -exec rm -f {} \;
cp -p %{SOURCE1} build.xml
sed -i 's/\r//' LICENSE.txt

%mvn_file antlr:antlr antlr

%build
ant -Dj2se.apidoc=%{_javadocdir}/java
cp work/lib/antlr.jar .
export CLASSPATH=.
%configure --without-examples
make CXXFLAGS="${CXXFLAGS} -fPIC" DEBUG=1 verbose=1
rm antlr.jar doc/{Makefile,Makefile.in}
chmod 0644 doc/*

cd lib/cpp
  doxygen doxygen.cfg
  find gen_doc -type f -exec chmod 0644 {} \;
cd -

%install
%mvn_artifact %{SOURCE3} work/lib/%{name}.jar
%mvn_alias %{name}:%{name} %{name}:%{name}all
%mvn_install -J work/api

install -Dp -m755 %{SOURCE2} $RPM_BUILD_ROOT%{_bindir}/antlr
install -d  $RPM_BUILD_ROOT%{_includedir}/%{name}
install -p -m644 lib/cpp/antlr/*.hpp $RPM_BUILD_ROOT%{_includedir}/%{name}
install -Dp -m644 lib/cpp/src/libantlr.a $RPM_BUILD_ROOT%{_libdir}/libantlr.a
install -p -m755 scripts/antlr-config $RPM_BUILD_ROOT%{_bindir}/antlr-config

%files tool -f .mfiles
%license LICENSE.txt
%{_bindir}/antlr

%files C++
%{_includedir}/%{name}
%{_libdir}/libantlr.a
%{_bindir}/antlr-config

%files C++-doc
%doc lib/cpp/gen_doc/html/

%files help -f .mfiles-javadoc
%doc doc/*

%changelog
* Wed dec 16 2020 Ge Wang <wangge20huawei.com> - 2.7.7-60
- Modify license 

* Wed Oct 21 2020 chengzihan <chengzihan2@huawei.com> - 2.7.7-59
- Remove subpackage python2-antlr

* Tue Jan 21 2020  chenli <chenli147@huawei.com> - 2.7.7-58
- Package init
